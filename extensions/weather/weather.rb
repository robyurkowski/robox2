require 'feedzirra'

module Robox
  class WeatherPlugin < Plugin

    def initialize(client)
      super(client)
      @client.add_hook(:command) {|m| route m[:target], m[:command], m[:user] }
    end

    def route(target, command, user)
      /\Aweather\Z/i.match(command) { regurgitate_weather(target, user) }
    end

    def regurgitate_weather(target, user)
      feed = ::Feedzirra::Feed.fetch_and_parse("http://www.weatheroffice.gc.ca/rss/city/on-174_e.xml")
      @client.say target, "#{user}: The current weather in Sudbury, ON is as follows:"
      lines = feed.entries[1].summary.sanitize.split("<br>")
      lines.map! {|l| l.gsub(/\A<b>.+?<\/b>/i, '') }
      response = "#{lines[2]} (feels like #{lines[6]}). #{lines[1]}. Winds #{lines[8]}. Barometer #{lines[3]}."
      @client.say target, response.gsub("&deg;", "*").gsub('\s+', ' ')
    end
  end
end
