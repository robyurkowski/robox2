module Robox
  class ChannelsPlugin < Plugin
    def initialize(client)
      super(client)

      @client.add_hook(:command) {|m| route m[:target], m[:command], m[:user] }
      @client.add_hook(:self_join) {|m| add_channel m[:channel] }
      @client.add_hook(:self_part) {|m| remove_channel m[:channel] }
    end

    def route(target, command, user)
      /\Aautojoin (.+?)\Z/i.match(command) do |m|
        channel = Channel.find_by_name(m[1])
        channel.update_attribute(:autojoin, true) unless channel.nil?
      end and return

      /\Aunautojoin (.+?)\Z/i.match(command) do |m|
        channel = Channel.find_by_name(m[1])
        channel.update_attribute(:autojoin, false) unless channel.nil?
      end and return

      /\Achannels list\Z/i.match(command) do |m|
        list_channels(user)
      end and return
    end

    def add_channel(channel)
      Channel.find_or_create_by_name(channel) {|c| c.autojoin = false }
    end

    def remove_channel(channel)
      channel = Channel.find_by_name(channel)
      channel.destroy unless channel.nil?
    end

    def list_channels(user)
      channels = Channel.order('name')
      @client.say user, channels.map{|c| "#{c.name}#{c.autojoin ? '+' : ''}" }.join(", ")
    end
  end
end
