module Robox
  class BasicCommandsPlugin < Plugin

    def initialize(client)
      super(client)
      @client.add_hook(:command) {|m| route m[:target], m[:command] }
    end

    ################################################################
    public
    ################################################################
    
    def route(target, command)
      # If we don't match the namespace and the possible command, terminate ASAP.
      return false unless /(join|part) #(.+)/i.match command

      if $1.downcase == 'join'
        @client.join("##{$2}")
        return true
      end
    end
      
  end
end
