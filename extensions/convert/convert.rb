module Robox
  class ConvertPlugin < Plugin
    
    def initialize(client)
      super(client)
      @client.add_hook(:command) {|m| route m[:target], m[:command], m[:user] }
    end
   
    def route(target, command, user)
      match_units = supported_units.join("|")
      regexp = /\A(-?\d+)(#{match_units}) (to|in) (#{match_units})\Z/i

      regexp.match(command) do |m|
        scalar, from_unit, to_unit = clean_matches(m)

        if to_unit == from_unit
          @client.say target, "#{user}: ^^"
        else
          if respond_to?("#{from_unit}_to_#{to_unit}")
            @client.say target, "#{user}: #{scalar}#{from_unit} ~= \u0002#{send("#{from_unit}_to_#{to_unit}".to_sym, scalar)}#{to_unit}\u000f"
          else
            @client.say target, "#{user}: Sorry, I don't support that conversion. I can convert amongst these units, though: #{match_units.split("|") * ', '}."
          end
        end
      end
    end

    def supported_units
      %w(c f k kgs lbs mi miles km kms)
    end

    def clean_matches(match)
      s, f, t = match[1], match[2], match[4]
      [f, t].map do |u| 
        u.gsub!(/\Ami\Z/i, 'miles')
        u.gsub!(/\Akm\Z/i, 'kms')
        u.downcase!
      end

      return s, f, t
    end


    def k_to_c(degrees)
      degrees.to_i - 273.15
    end

    def c_to_k(degrees)
      degrees.to_i + 273.15
    end

    def c_to_f(degrees)
      degrees.to_i * 1.8 + 32
    end

    def k_to_f(degrees)
      c_to_f(k_to_c(degrees.to_i))
    end
    
    def f_to_c(degrees)
      (degrees.to_i - 32) / 1.8
    end

    def f_to_k(degrees)
      c_to_k(f_to_c(degrees.to_i))
    end

    def lbs_to_kgs(pounds)
      pounds.to_i / 2.2
    end

    def kgs_to_lbs(pounds)
      pounds.to_i * 2.2
    end
    
    def miles_to_kms(distance)
      distance.to_i * 1.6
    end

    def kms_to_miles(distance)
      distance.to_i * 0.621371
    end
    
  end
end
