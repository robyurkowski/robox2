require 'active_support/core_ext/array'

module Robox
  class DefinitionsPlugin < Plugin
    def initialize(client)
      super(client)
      @client.add_hook(:privmsg) {|m| watch_definition m[:target], m[:message], m[:user] }
      @client.add_hook(:command) {|m| route m[:target], m[:command], m[:user] }
    end

    def watch_definition(target, message, user)
      /\A((.+?):\s)?\!(.+?)\??\Z/i.match(message) do |m|
        definition = find_definition(m[3])
        responder = m[2] ? m[2] + ": " : ""
        @client.say target, "#{responder}#{definition.value}" if definition
      end and return
    end


    def route(target, message, user)
      /\Adef(ine)? (.+?) (as|is) (.+?)\Z/i.match(message) do |m|
        create_or_update_definition(m[2], m[4])
        @client.say target, "#{user}: Okay, when people ask about '#{m[2]}', I'll say '#{m[4]}.'"
      end and return

      /\Aforget (.+?)\Z/i.match(message) do |m|
        definition = find_definition(m[1])
        definition.destroy

        @client.say target, "#{user}: Okay, I've forgotten about #{m[1]}."
      end and return

      /\Adefinitions list\Z/i.match(message) do |n|
        list_definitions.in_groups_of(20, false) do |group|
          @client.say user, group.join(" ")
        end
      end
    end

    def create_or_update_definition(k, v)
      definition = ::Definition.find_or_initialize_by_name(k.to_s)
      highlight definition.inspect
      definition.update_attributes!(:value => v.to_s)
    end

    def find_definition(k)
      ::Definition.find_by_name(k.to_s)
    end

    def list_definitions
      ::Definition.order('name ASC').pluck('name')
    end
  end
end
