require 'spec_helper'
require 'colorize'
require 'logging'

class Dummy
  include Logging
end

describe "Logging" do
  let(:sink) { StringIO.new }
  let(:dummy) { Dummy.new }
  let(:message) { "This is a long, stupid string." }

  before(:each) do
    dummy.instance_variable_set(:@pipe, sink)
  end

  describe "#debug" do
    it "does nothing when $debug is false" do
      $debug = false
      dummy.debug message
      sink.string.should == ""
      $debug = true
    end
  end

  before(:all) { $debug = true }
  {:debug => :red, :log => :cyan, :chat => :magenta, :highlight => :yellow}.each_pair do |method, color|
    describe "##{method.to_s}" do
      it "echoes #{color}" do
        dummy.send(method, message.send(color))
        sink.string.should == (message.send(color) + "\n")
      end
    end
  end
end
