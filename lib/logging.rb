require 'colorize'

module Logging

  def debug(message)
    if $debug
      output message.red
    end
  end
  
  def log(message)
    output message.cyan
  end

  def chat(message)
    output message.magenta
  end

  def highlight(message)
    output message.yellow
  end

  private
  def logger
    @pipe ||= $stdout
  end

  def output(message)
    logger.write message
    logger.write "\n"
  end

end
