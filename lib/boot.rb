require 'bundler/setup'
require 'active_record'

Dir[File.join(File.dirname(__FILE__), 'ext', '*.rb')].each {|f| require f }
require 'client'
require 'plugin'

Dir[File.join(File.dirname(__FILE__), 'models', '*.rb')].each {|f| require f }
Dir[File.join(File.dirname(__FILE__), '..', 'extensions', '**', '*.rb')].each {|f| puts "loading #{f}"; load f }

ActiveRecord::Base.establish_connection({
  :adapter          => 'sqlite3',
  :database         => 'db/robox.sqlite3',
  :pool             => 5,
  :timeout          => 5000
})

